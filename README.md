# SpawnSnowballs

SpawnSnowballs is simple little plugin which I created for my own server.
It allows players to fight each other using snowballs. Players can pickup snowballs
from snow lying on the ground, but here are also configurable limitations so players
cannot farm snowballs on spawn!