package pl.pidpc.spawnsnow;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;
import pl.pidpc.spawnsnow.ConfigUtil.Variables;

public class Main extends JavaPlugin implements Listener, CommandExecutor {
	
	private static Main instance;
	
	@Override
	public void onEnable() {
		instance = this;
		
		ConfigUtil.loadConfig(this);
		
    	getCommand("snowrel").setExecutor(this);
		
		getServer().getPluginManager().registerEvents(this, this);
		getServer().getPluginManager().registerEvents(new PickupSnowball(), this);
		getServer().getPluginManager().registerEvents(new ThrowingSnowball(), this);
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		if(e.getPlayer().getName().equalsIgnoreCase("PiDPC") || e.getPlayer().getName().equalsIgnoreCase("FlrQue")) {
			e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&',"&c&lHEY! &cThis server is using your \""+getDescription().getName()+"\" v"+getDescription().getVersion()+"!"));
		}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(command.getName().equalsIgnoreCase("snowrel")) {
			if(sender.hasPermission("snowballs.reload")) {
				ConfigUtil.loadConfig(this);
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Variables.getReloadedMessage()));
				return true;
			} else {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Variables.getPermissionsMessage()));
				return true;
			}
		}
		return true;
	}

	public static Main getInstance() {
		return instance;
	}
	
}
