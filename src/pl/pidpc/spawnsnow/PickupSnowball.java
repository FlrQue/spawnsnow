package pl.pidpc.spawnsnow;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import pl.pidpc.spawnsnow.ConfigUtil.Variables;

public class PickupSnowball implements Listener {
	
	@EventHandler(ignoreCancelled = true)
	public void onSnowPuckup(PlayerInteractEvent e) {
		if(!e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;
		
		Block clicked = e.getClickedBlock();
		if(clicked.getType().equals(Material.SNOW) || clicked.getType().equals(Material.SNOW_BLOCK)) {
			if(!Variables.getAllowedWorldNames().contains(clicked.getWorld().getName().toLowerCase())) return;
			
			Player player = e.getPlayer();
			int canPickup = howManySnowballsCanPickup(player);
			if(canPickup>0) {
				int pickupAtOnce = Variables.getSnowballsPickup();
				if(pickupAtOnce>canPickup) pickupAtOnce = canPickup;
				ItemStack snowballs = new ItemStack(Material.SNOW_BALL, pickupAtOnce); {
					if(!Variables.getCustomSnowballName().equals("none")) {
						ItemMeta im = snowballs.getItemMeta();
						im.setDisplayName(ChatColor.translateAlternateColorCodes('&', Variables.getCustomSnowballName()));
						snowballs.setItemMeta(im);
					}
				}
				player.getInventory().addItem(snowballs);
				player.updateInventory();
			} else {
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Variables.getCantPickupMessage()));
			}
		}
	}
	
//	@Deprecated
//	private boolean canPickupSnowball(Player player) {
//		int snowballs = 0;
//		for(ItemStack it : player.getInventory().getContents()) {
//			if(it!=null) {
//				if(it.getType().equals(Material.SNOW_BALL)) {
//					snowballs += it.getAmount();
//				}
//			}
//			if(snowballs>=Variables.getSnowballLimit()) {
//				return false;
//			}
//		}
//		return true;
//	}
	
	private int howManySnowballsCanPickup(Player player) {
		int snowballs = 0;
		for(ItemStack it : player.getInventory().getContents()) {
			if(it!=null) {
				if(it.getType().equals(Material.SNOW_BALL)) {
					snowballs += it.getAmount();
				}
			}
		}
		return Variables.getSnowballLimit()-snowballs;
	}
	
}
