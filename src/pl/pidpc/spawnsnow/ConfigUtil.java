package pl.pidpc.spawnsnow;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class ConfigUtil {
	
	protected static class Variables {
		private static List<String> allowed_worlds;
		private static List<String> worldguard_bypass_worlds;
		private static String snowball_display;
		private static String cant_pickup_msg;
		private static String permissions_msg;
		private static String reloaded_msg;
		private static int snowball_pickup;
		private static int snowball_limit;
		private static boolean citizens_compatible;

		protected static List<String> getAllowedWorldNames() {
			return allowed_worlds;
		}

		protected static List<String> getWorldGuardBypassWorldNames() {
			return worldguard_bypass_worlds;
		}

		protected static String getCustomSnowballName() {
			return snowball_display;
		}
		
		protected static String getCantPickupMessage() {
			return cant_pickup_msg;
		}
		
		protected static String getPermissionsMessage() {
			return permissions_msg;
		}
		
		protected static String getReloadedMessage() {
			return reloaded_msg;
		}
		
		protected static int getSnowballsPickup() {
			return snowball_pickup;
		}
		
		protected static int getSnowballLimit() {
			return snowball_limit;
		}
		
		protected static boolean shouldBeCitizensCompatible() {
			return citizens_compatible;
		}
	}
	
	private static File configFile;
	private static FileConfiguration config = new YamlConfiguration();
	
	public static void loadConfig(JavaPlugin plugin) {
		configFile = new File(plugin.getDataFolder(), "config.yml");
		if(!configFile.exists()) {
			configFile.getParentFile().mkdirs();
			plugin.saveResource("config.yml", false);
		}
		
		try {
			config.load(configFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
		
		importConfigSettings();
	}
	
	private static void importConfigSettings() {
		
		if(config.get("allowed_worlds")!=null) {
			Variables.allowed_worlds = config.getStringList("allowed_worlds");
		} else {
			Main.getInstance().getLogger().log(Level.WARNING, "Error in config.yml - allowed_worlds. Loading defaults.");
			Variables.allowed_worlds = new ArrayList<String>(); //no worlds means no worlds deamn!
		}
		
		if(config.get("worldguard_bypass_worlds")!=null) {
			Variables.worldguard_bypass_worlds = config.getStringList("worldguard_bypass_worlds");
		} else {
			Main.getInstance().getLogger().log(Level.WARNING, "Error in config.yml - worldguard_bypass_worlds. Loading defaults.");
			Variables.worldguard_bypass_worlds = new ArrayList<String>(); //no worlds means no worlds deamn!
		}
		
		if(config.get("snowball_display")!=null) {
			Variables.snowball_display = config.getString("snowball_display");
		} else {
			Main.getInstance().getLogger().log(Level.WARNING, "Error in config.yml - snowball_display. Loading defaults.");
			Variables.snowball_display = "none";
		}
		
		if(config.get("cant_pickup_msg")!=null) {
			Variables.cant_pickup_msg = config.getString("cant_pickup_msg");
		} else {
			Main.getInstance().getLogger().log(Level.WARNING, "Error in config.yml - cant_pickup_msg. Loading defaults.");
			Variables.cant_pickup_msg = "&4Can't pickup more snowballs!";
		}
		
		if(config.get("reloaded_msg")!=null) {
			Variables.reloaded_msg = config.getString("reloaded_msg");
		} else {
			Main.getInstance().getLogger().log(Level.WARNING, "Error in config.yml - reloaded_msg. Loading defaults.");
			Variables.reloaded_msg = "&2SpawnSnoballs reloaded!";
		}
		
		if(config.get("permissions_msg")!=null) {
			Variables.permissions_msg = config.getString("permissions_msg");
		} else {
			Main.getInstance().getLogger().log(Level.WARNING, "Error in config.yml - permissions_msg. Loading defaults.");
			Variables.permissions_msg = "&4You do not have permission for that!";
		}
		
		if(config.get("snowball_limit")!=null) {
			Variables.snowball_limit = config.getInt("snowball_limit");
		} else {
			Main.getInstance().getLogger().log(Level.WARNING, "Error in config.yml - snowball_limit. Loading defaults.");
			Variables.snowball_limit = 4;
		}
		
		if(config.get("snowball_pickup")!=null) {
			Variables.snowball_pickup = config.getInt("snowball_pickup");
		} else {
			Main.getInstance().getLogger().log(Level.WARNING, "Error in config.yml - snowball_pickup. Loading defaults.");
			Variables.snowball_pickup = 1;
		}
		
		if(config.get("citizens_compatible")!=null) {
			Variables.citizens_compatible = config.getBoolean("citizens_compatible");
		} else {
			Main.getInstance().getLogger().log(Level.WARNING, "Error in config.yml - citizens_compatible. Loading defaults.");
			Variables.citizens_compatible = true;
		}
	}
	
}
