package pl.pidpc.spawnsnow;


import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import pl.pidpc.spawnsnow.ConfigUtil.Variables;

public class ThrowingSnowball implements Listener {
		  
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
	public void onSnowball(EntityDamageByEntityEvent e) {
		if(!Variables.getWorldGuardBypassWorldNames().contains(e.getEntity().getWorld().getName())){
			return;
		}
		
		if(!(e.getDamager() instanceof Snowball)) return;
		if(Variables.shouldBeCitizensCompatible()) {
			if(e.getEntity().hasMetadata("NPC")) return;
		}
		if(!(e.getEntity() instanceof Player)) return;
		
		Player target = (Player)e.getEntity();
		Snowball snowball = (Snowball)e.getDamager();
		
		target.setVelocity(snowball.getVelocity());
		target.damage(0.0);
	}

}
